from distutils.core import setup

setup \
(
    name='yn-input',
    version='1.0',
    packages=['yn_input'],
    url='https://gitlab.com/Hares/yn-input',
    license='MIT',
    author='USSX Hares / Peter Zaitcev',
    author_email='ussx.hares@yandex.ru',
    description='Python simple tool that confirms action from user via console.',
    keywords=['yn', 'input', 'console', 'confirm'],
)
