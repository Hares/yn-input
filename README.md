# Python Y/N input
Simple tool that asks user's confirmation

## Installation:
Run as sudo/administrator: `pip install yn-input`

## Usage:
`from yn_input import yn_input`
`if (yn_input('Please, confirm')):`
`    print("Confirmed!")`
`else:`
`    print("Not confirmed :(")`
